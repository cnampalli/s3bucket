# Template repository
This is a template repository to buid the various data add-ons that poll the REST APIs to populate the splunk server.

In order to build a new add-on
 * Clone this repository
 * Replace s3 in all files and filenames
 * Implement what is specific to your addons, in particuliar:
   - the InputConfiguration
   - the responseHandler
   - the authenticationHandler if required


# Devops s3 Addon

## Configuration

There are 2 different ways of configuring the add-on. You can use the setup from within the add-on or use a configuration file and generate the inputs.conf before installing the add-on as described below.

* Configure the *config.properties* with the following properties :
  
  - host : URL of the s3 server (ex : s3.accenture.com)
  - protocol : protocol to use, http or https
  - auth_type : Type of authentication used to access the s3 server (ex :basic)
  - auth_user : Username used to log in
  - auth_password : Password used to log in, this must be a base64 encoded string (use ``` echo -n 'password' | base64``` to encode)
  - interval : 120 (in seconds)

* Additional optional parameters may also be over-written:
  - sourcetype : name of the sourcetype (default is s3_api)
  - index : Name of the index we want to send the data (default is aaam_devops_s3_idx)
  - interval : Polling time between different requests (default is 300s.)

* You may add as many sections as you want to include different information or ping different servers, the category name of the config file **MUST** be different

```
[s3]
host = s3.accenture.com
auth_type = basic
auth_user =
auth_password =
interval = 86400

[s3-2]
host = s3.accenture.com
auth_type = basic
auth_user =
auth_password =
interval = 86400
```
   
* Generate the final configuration :
  
    python generate_s3_config.py <config>
    
* Check the *default/inputs.conf* generated has all of your configuration parameters


## Package

* Create a tarball of this repository :

    tar czvf aaam-devops-s3-addon.tar.gz aaam-devops-s3-addon
    
## Installation 

* Upload this archive into splunk with the Manage Apps UI screen
* Don't forget to create the index **aaam_devops_s3_idx** if it is not done

## Search 

* data is present in the **aaam_devops_s3_idx** index