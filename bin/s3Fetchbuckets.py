#!/usr/bin/python
import base64
import sys,logging,os,time,re,threading
import logging
import ConfigParser
import boto3
import requests
import json

ADDON_NAME='s3'

acc_name = sys.argv[2]

def create_s3_client(key_id, secret_key):
    try:
        logging.info("Creating boto3 aws session")
        session = boto3.session.Session(aws_access_key_id=key_id, aws_secret_access_key=secret_key)
        return session.client('s3')
    except Exception as ex:
        logging.debug(ex)

def get_conf_path():
    logging.info("Fetching Auth data")
    appdir = "/opt/splunk/etc/apps/aaam-devops-%s-addon" % ADDON_NAME
    apikeyconfpath = os.path.join(appdir, "default", "passwords.conf")
    return apikeyconfpath

def get_auth_conf(stanza):
    apikeyconfpath = get_conf_path()
    config = ConfigParser.RawConfigParser()
    config.read(apikeyconfpath)
    configArray = []

    for section in config.sections():
        if section == stanza:
            configArray.append(config.get(section, 'password'))
            configArray.append(config.get(section, 'credential'))

    return configArray

def decrypt_secret_key(auth_array):
    logging.info("starting decrypt secret key process")
    fsencoding = sys.getfilesystemencoding()
    decoded_pwd = (auth_array[0]).decode(fsencoding)
    auth_array[0] = decoded_pwd
    return auth_array

def fetch_buckets(s3_client):
    logging.info("Fetching buckets")
    auth_array = get_auth_conf(acc_name)
    decrypted_auth = decrypt_secret_key(auth_array)
    s3_client = create_s3_client(decrypted_auth[1], decrypted_auth[0])
    response = s3_client.list_buckets()
    return response

if __name__ == "__main__" :
    logging.basicConfig(filename='bucket_fetcher.log',level=logging.INFO)
    logging.info("Starting application...")
    if acc_name:
        try:
            logging.info("Attempting to fetch buckets list")
            raw_buckets = fetch_buckets(acc_name)
            print raw_buckets
        except Exception as ex:
            logging.debug(ex)
    else:
        print "error " + str(acc_name)
    
            
