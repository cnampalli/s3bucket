#!/usr/bin/python
import os
import subprocess
import sys
import logging
wrapped_script="s3Fetchbuckets.py"

_SPLUNK_HOME=os.environ['SPLUNK_HOME']
_SPLUNK_PYTHON_PATH = os.environ['PYTHONPATH']
os.environ['LD_LIBRARY_PATH']='' #hack to stop splunk libries loading instead of system

if sys.platform == "win32":
    logfilename = 'C:\\tmp\\searchcommand2_app.log'
    _APP_BIN_HOME=_SPLUNK_HOME + "\\etc\\apps\\aiam-ml-core\\bin"
    _NEW_PYTHON_PATH = "C:\\opt\\Anaconda2\\python"
    os.environ['PYTHONPATH'] = _NEW_PYTHON_PATH
    my_process = _APP_BIN_HOME + "\\" + wrapped_script
elif sys.platform == "darwin" or sys.platform == "linux" or sys.platform == "linux2":
    logfilename = '/tmp/searchcommand2_app.log'
    _APP_BIN_HOME = _SPLUNK_HOME + "/etc/apps/aaam-devops-s3-addon/bin"
#    _NEW_PYTHON_PATH = _SPLUNK_HOME + "/etc/apps/aiam-ml-core/python/bin/python"
    _NEW_PYTHON_PATH =  "/usr/bin/python"
    os.environ['PYTHONPATH'] = _NEW_PYTHON_PATH
    my_process = _APP_BIN_HOME + "/" + wrapped_script

logging.basicConfig(filename=logfilename, level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
mylogger = logging.getLogger("ourlogger")
mylogger.debug("wrapper: sys.argv=%s" % sys.argv)

argstr=''
for thing in sys.argv:
    argstr=argstr + " " + thing

def run (acc_name):
    args = []
    args.append(acc_name)
    p = subprocess.Popen([os.environ['PYTHONPATH'], my_process, _NEW_PYTHON_PATH ] + args , stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=sys.stderr)
    out, err = p.communicate()
    return out
