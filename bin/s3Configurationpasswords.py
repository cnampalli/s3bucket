import ConfigParser
import os.path
from abc import ABCMeta, abstractmethod
import sys
import base64

#############
# Class used to create passwords.conf file
#############

class AbstractAppConfiguration:
  __metaclass__ = ABCMeta

  def __init__(self, app_dir=None):
    if not app_dir:
      app_name = __file__.split(os.sep)[-3]
      app_dir = os.path.join(os.environ["SPLUNK_HOME"], 'etc', 'apps', app_name)
    self.configPasswordsFile = os.path.join(app_dir, 'default', 'passwords.conf')
    self.configFile = os.path.join(app_dir, 'config_passwords.properties')

  def addParameters(self, inputConfig, inputSection, outputConfig, outputSection, parameters):
    """ Adds a parameters to a config file using an input config, add a hashmap containg default values if the value is missing in the input
        inputConfig -- ConfigParser to read the value from
        inputSection -- Name of the input section to read the value from
        outputConfig -- Output configuration object
        outputSection -- Section of the outputConfig in which to write the parameter
        parameters -- Hashmap containing default values
    """
    for parameter in parameters:
      if inputConfig.has_option(inputSection, parameter):
        parameterValue = inputConfig.get(inputSection, parameter)
        outputConfig.set(outputSection, parameter, parameterValue)
      elif parameters[parameter]:
        parameterValue = parameters[parameter]
        outputConfig.set(outputSection, parameter, parameterValue)
      else:
        print "WARN: parameter {0} is missing in section {1}".format(parameter, outputSection)  

  def addAuthenticationConfiguration(self, inputConfig, inputSection, outputConfig, outputSection):
    """ Adds specific authentication parameters
    """
    auth_type = inputConfig.get(inputSection, 'auth_type')
    outputConfig.set(outputSection, 'auth_type', auth_type)
    parameters ={
      'none'   : [],
      'basic'  : ['auth_user', 'password'],
      'digest' : ['auth_user', 'auth_password'],
      'oauth1' : ['oauth1_client_key', 'oauth1_client_secret', 'oauth1_access_token', 'oauth1_access_token_secret'],
      'oauth2' : ['oauth2_token_type', 'oauth2_access_token', 'oauth2_refresh_token', 'oauth2_refresh_url', 'oauth2_refresh_props', 'oauth2_client_id', 'oauth2_client_secret' ],
      'custom' : ['password']
    }[auth_type]
    self.addParameters (inputConfig, inputSection, outputConfig, outputSection, parameters)

  @abstractmethod
  def handleSection():
    pass

  def createInputsConfiguration(self, configPath=None, fileWriteMode='w'):
    """Creates or appends to the existing configuration file
    """
    if not configPath:
      configPath = self.configFile
    if os.path.isfile(str(configPath)) :
      inputConfig = ConfigParser.RawConfigParser()
      inputConfig.read(configPath)
      with open(self.configPasswordsFile, fileWriteMode) as configPasswordsFile:
        outputConfig = ConfigParser.RawConfigParser()
        for inputSection in inputConfig.sections():
          self.handleSection(inputConfig, inputSection, outputConfig)
        outputConfig.write(configPasswordsFile)
    else:
      print ('%s is not  a correct input file' % configPath)

  def appendToInputConfiguration(self, inputConfig):
    """Creates or appends to the existing configuration file
    """
    with open(self.configPasswordsFile, 'a') as configPasswordsFile:
      outputConfig = ConfigParser.RawConfigParser()
      for inputSection in inputConfig.sections():
        self.handleSection(inputConfig, inputSection, outputConfig)

      # Encode password
      for section in outputConfig.sections():
        encoded_pwd = base64.b64encode(config.get(section, 'password'))
        config.set(section, 'password', encoded_pwd)
      outputConfig.write(configPasswordsFile)

  def readConfig(self):
    fsencoding = sys.getfilesystemencoding()  
    if os.path.isfile(str(self.configFile)) :
      config = ConfigParser.RawConfigParser()
      config.read(self.configFile)

      # Decode passwords
      for section in config.sections():
        encoded_pwd = base64.b64decode(config.get(section, 'password'))
        #encoded_pwd = (config.get(section, 'password')).decode(fsencoding)		
        config.set(section, 'password', encoded_pwd)
      return config
    else:
      print ('%s is not  a correct input file' % self.configFile)

  def writeConfig(self, config):
    fsencoding = sys.getfilesystemencoding()  
    # Encode password
    for section in config.sections():
      encoded_pwd = base64.b64encode(config.get(section, 'password'))
      #encoded_pwd = (config.get(section, 'password')).encode(fsencoding)
      config.set(section, 'password', encoded_pwd)

    with open(self.configFile, 'w') as configFile:
      config.write(configFile)

####
# Specific Add on
####
class AppConfiguration(AbstractAppConfiguration):
  def __init__(self, app_dir=None):
    super(AppConfiguration, self).__init__(app_dir)
    self.section = '{aws_account}'
    self.default_configuration = {
        'password' : encoded_pwd = base64.b64encode('password'),#'password',
        'region' : 'region',
        'credential' : 'credential'
    }

  def handleSection(self, inputConfig, inputSection, outputConfig):
    #aws_account = inputConfig.get(inputSection,'aws_account')
    aws_account = inputConfig.get(inputSection,'account')
    section = self.section.format(aws_account=aws_account)
    outputConfig.add_section(section)      
    outputConfig.set(section, 'region')
    self.addParameters(inputConfig, inputSection, outputConfig, section, self.default_configuration)
