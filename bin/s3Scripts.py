import json
import splunk.rest
import sys
sys.path.insert(0, '/opt/splunk/etc/apps/aaam-devops-s3-addon/bin/')
import s3Fetchbucketswrapper

#####
# REST endpoint setup, available on /setup (see restmap.conf)
#####
class ScriptHandler(splunk.rest.BaseRestHandler):

    def __init__(self, method, requestInfo, responseInfo, sessionKey):
        splunk.rest.BaseRestHandler.__init__(self,
            method, requestInfo, responseInfo, sessionKey)

    def writeJson(self, data):
        self.response.setStatus(200)
        self.response.setHeader('content-type', 'application/json')
        self.response.write(json.dumps(data))
    
    def formatJsonString(self, string):
        string = string.split("Buckets")[1].split("[")[1].split("]")[0].replace("'",'"')
        string = string.replace('u"','"')
        string = string.replace("datetime.d",'"datetime.d')
        string = "[" + string.replace("tzlocal())",'tzlocal())"') + "]"
        return string
    
    def handle_GET(self):
        acc_name = self.request['query']['acc_name']
        string = s3Fetchbucketswrapper.run(acc_name)
        string = self.formatJsonString(string)
        bucketsJson = json.loads(string)
        self.writeJson(bucketsJson)