from splunksdc import logging
from splunksdc.event import EventStream
from splunksdc.collector import SimpleCollectorV1
from splunksdc.multitasking import JobPipeline

__version__ = '0.8'
__all__ = ['SimpleCollectorV1', 'logging', 'JobPipeline']
