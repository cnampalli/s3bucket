import time
import Queue
import thread
import threading
import multiprocessing

from solnlib import timer_queue
from splunksdc import logging


logger = logging.getLogger('splunksdc')


class TaskEntry(object):
    def __init__(self, params, timer):
        self._params = params
        self._timer = timer

    @property
    def params(self):
        return self._params

    @property
    def timer(self):
        return self._timer


class TimerTaskScheduler(object):
    def __init__(self, create_worker, worker_has_done, destroy_worker):
        self._create_worker = create_worker
        self._worker_has_done = worker_has_done
        self._destroy_worker = destroy_worker
        self._tasks = dict()
        self._pending_tasks = list()
        self._max_concurrent = multiprocessing.cpu_count()
        self._max_concurrent = min(self._max_concurrent, 16)
        self._running_tasks = dict()
        self._timer_queue = timer_queue.TimerQueueStruct()

    def idle(self):
        if len(self._tasks) == 0:
            return True
        return False

    def add_task(self, name, params, interval):
        if name in self._tasks:
            return False

        def pending_task():
            self._pending_tasks.append(name)

        now = time.time()
        timer = self._timer_queue.add_timer(pending_task, now, interval)
        self._tasks[name] = TaskEntry(params, timer)
        return True

    def set_max_concurrent(self, value):
        self._max_concurrent = value

    def _handle_running_workers(self, now=None):
        if now is None:
            now = int(time.time())
        competed_tasks = set()
        for name, worker in self._running_tasks.items():
            if self._worker_has_done(worker):
                competed_tasks.add(name)
                self._destroy_worker(worker)
        for name in competed_tasks:
            del self._running_tasks[name]
            entry = self._tasks[name]
            if entry.timer.interval == 0:
                del self._tasks[name]

    def _schedule(self):
        wait_time = 1
        if self._has_work_slot():
            self._timer_queue.check_and_execute()

        while len(self._pending_tasks) and self._has_work_slot():
            name = self._pending_tasks.pop()
            if name not in self._running_tasks:
                task = self._tasks[name]
                worker = self._create_worker(name, task.params)
                self._running_tasks[name] = worker
        return wait_time

    def run(self):
        self._handle_running_workers()
        wait_time = self._schedule()
        time.sleep(wait_time)
        return wait_time

    def _has_work_slot(self):
        return len(self._running_tasks) < self._max_concurrent


class JobPipelineStop(object):
    def __init__(self, exhausted):
        self.exhausted = exhausted


class JobPipeline(object):

    def __init__(self, delegate):
        self._delegate = delegate
        self._completed_queue = Queue.Queue()
        self._pending_queue = Queue.Queue()
        self._stopped = threading.Event()

    def _spawn(self):
        count = multiprocessing.cpu_count() * 2
        count = min(count, 32)
        workers = list()
        for _ in range(count):
            resources = self._delegate.allocate()
            if not isinstance(resources, tuple):
                resources = resources,
            worker = threading.Thread(
                target=self._worker_procedure,
                args=resources
            )
            worker.daemon = True
            workers.append(worker)
        for worker in workers:
            worker.start()
        return workers

    def run(self, portal, checkpoint):
        workers = self._spawn()
        exhausted = False

        for item in self._delegate.discover(checkpoint):
            if isinstance(item, JobPipelineStop):
                exhausted = item.exhausted
                break

            jobs = item
            if not jobs:
                continue

            number_of_pending = 0
            for job in jobs:
                self._pending_queue.put(job)
                number_of_pending += 1

            while number_of_pending > 0:
                if self._delegate.is_aborted():
                    break
                try:
                    job, result = self._completed_queue.get(timeout=3)
                    self._delegate.done(portal, checkpoint, job, result)
                    number_of_pending -= 1
                except Queue.Empty:
                    pass
                
            if self._delegate.is_aborted():
                break

        self._stopped.set()

        for worker in workers:
            worker.join(10.0)

        return exhausted

    def _worker_procedure(self, *args):
        while not self._stopped.is_set():
            try:
                job = self._pending_queue.get(timeout=3)
                result = self._delegate.do(job, *args)
                self._completed_queue.put((job, result))
            except Queue.Empty:
                pass
        return

