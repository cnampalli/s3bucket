import copy
from splunklib.modularinput.event import ET


class SimpleEventWriter(object):
    def __init__(self, lock, dev):
        self._lock = lock
        self._dev = dev

    def write(self, stream):
        text = stream.render()
        with self._lock:
            self._dev.write(text)
            self._dev.flush()


class EventStream(object):
    _VALID_STREAM_KEYS_ = {
        'source',
        'sourcetype',
        'host',
        'index',
        'stanza'
    }

    def __init__(self, **kwargs):
        for key in kwargs.iterkeys():
            if key not in self._VALID_STREAM_KEYS_:
                raise ValueError('{} is an invalid key'.format(key))

        self._root = ET.Element('stream')
        self._prototype = ET.Element('event')

        stanza = kwargs.pop('stanza', None)
        if stanza:
            self._prototype.set("stanza", stanza)

        for key, value in kwargs.iteritems():
            if value is not None:
                element = ET.SubElement(self._prototype, key)
                element.text = value

    def append(self, data, **kwargs):
        event = copy.copy(self._prototype)
        element = ET.SubElement(event, 'data')
        element.text = data
        time = kwargs.pop('time', None)
        if time:
            element = ET.SubElement(event, "time")
            element.text = str(time)
        self._root.append(event)

    def render(self):
        return ET.tostring(self._root)

