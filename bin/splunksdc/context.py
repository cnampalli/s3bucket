import urlparse
import logging
from splunksdc.utils import get_log_folder
from splunksdc.utils import get_checkpoint_folder


def build_context_from_url(server_uri, stanza):
    return Context.from_url(server_uri, stanza)


def build_context_from_stream(stream):
    return Context.from_stream(stream)


class Stanza(object):
    def __init__(self, kind, name, content):
        self.kind = kind
        self.name = name
        self.content = content
        self.content = {
            key: value.strip()
            for key, value in self.content.items()
            if value is not None
        }


class Context(object):
    @classmethod
    def from_url(cls, server_uri, stanza):
        logging.setup_null_handler()

        from splunklib.client import Service

        parts = urlparse.urlparse(server_uri)
        scheme = parts.scheme
        host = parts.hostname
        port = parts.port

        service = Service(
            scheme=scheme,
            host=host,
            port=port,
            username=parts.username,
            password=parts.password,
        )
        service.login()

        context = Context()
        context._server_scheme = scheme
        context._server_host = host
        context._server_port = port
        context._token = service.token
        context._log_dir = get_log_folder()

        kind, name = stanza.split(':')
        name = name[2:]
        for item in service.inputs:
            if item.kind != kind:
                continue
            if item.name != name:
                continue
            stanza = Stanza(kind, name, item.content)
            context._inputs.append(stanza)

        context._checkpoint_dir = get_checkpoint_folder(kind)
        return context

    @classmethod
    def from_stream(cls, stream):
        from splunklib.modularinput.input_definition import InputDefinition

        definition = InputDefinition.parse(stream)

        metadata = definition.metadata
        inputs = definition.inputs
        parts = urlparse.urlparse(metadata['server_uri'])
        scheme = parts.scheme
        host = parts.hostname
        port = parts.port
        token = metadata['session_key']

        context = cls()
        context._server_scheme = scheme
        context._server_host = host
        context._server_port = port
        context._token = token
        context._log_dir = get_log_folder()

        for stanza, content in inputs.iteritems():
            kind, name = stanza.split(':')
            name = name[2:]
            stanza = Stanza(kind, name, content)
            context._inputs.append(stanza)

        context._checkpoint_dir = metadata['checkpoint_dir']
        return context

    def __init__(self):
        self._server_scheme = None
        self._server_host = None
        self._server_port = None
        self._token = None
        self._checkpoint_dir = None
        self._log_dir = None
        self._inputs = list()

    @property
    def server_scheme(self):
        return self._server_scheme

    @property
    def server_host(self):
        return self._server_host

    @property
    def server_port(self):
        return self._server_port

    @property
    def token(self):
        return self._token

    @property
    def checkpoint_dir(self):
        return self._checkpoint_dir

    @property
    def log_dir(self):
        return self._log_dir

    @property
    def inputs(self):
        return self._inputs
