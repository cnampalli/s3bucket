import os
import time

def get_splunk_root():
    path = os.path.expanduser('~/splunk')
    if 'SPLUNK_HOME' in os.environ:
        path = os.environ["SPLUNK_HOME"]
    return path


def get_log_folder():
    root = get_splunk_root()
    path = os.path.join(root, 'var', 'log', 'splunk')
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def get_checkpoint_folder(schema):
    root = get_splunk_root()
    path = os.path.join(root, 'var', 'lib', 'splunk', 'modinputs', schema)
    if not os.path.exists(path):
        os.makedirs(path)
    return path


class FSLock(object):
    @classmethod
    def open(cls, path):
        if os.name == 'nt':
            import msvcrt

            def _lock(fd):
                msvcrt.locking(fd, msvcrt.LK_NBLCK, 1024)

            def _unlock(fd):
                msvcrt.locking(fd, msvcrt.LK_UNLCK, 1024)

            lock = _lock
            unlock = _unlock
        else:
            import fcntl

            def _lock(fd):
                fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)

            def _unlock(fd):
                fcntl.flock(fd, fcntl.LOCK_UN)

            lock = _lock
            unlock = _unlock

        return cls(path, lock, unlock)

    def __init__(self, path, lock, unlock):
        flag = os.O_RDWR | os.O_CREAT | os.O_TRUNC
        self._fd = os.open(path, flag)
        self._lock = lock
        self._unlock = unlock

    def acquire(self):
        self._lock(self._fd)

    def release(self):
        self._unlock(self._fd)

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()


class ParentWatcher(object):
    def __init__(self):
        self._last_modified = 0.0
        self._last_result = True
        self._check_alive = self._check_alive_mock
        if os.name == 'posix':
            self._check_alive = self._check_alive_posix

    def is_alive(self):
        now = time.time()
        if now - self._last_modified > 30.0:
            self._last_result = self._check_alive()
            self._last_modified = now
        return self._last_result

    @classmethod
    def _check_alive_posix(cls):
        return os.getppid() != 1

    @classmethod
    def _check_alive_mock(cls):
        return True




