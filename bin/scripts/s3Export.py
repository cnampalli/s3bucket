import sys
import os
import subprocess
import datetime
import random
import string
import socket
import logging
import ConfigParser
import boto3

ADDON_NAME='s3'
SPLUNK_HOME = os.environ.get("SPLUNK_HOME")

def read_passwordConf():
    cred_dict = {}
    app_name = __file__.split(os.sep)[-4]
    app_dir = os.path.join(SPLUNK_HOME, 'etc', 'apps', app_name)
    pswd_input_file = os.path.join(app_dir, 'default', 'passwords.conf')
    config = ConfigParser.ConfigParser()
    config.read(pswd_input_file)
    for section in config.sections():
        options = config.options(section)
        for option in options:
            try:
                if option == "region" and config.get(section, option) != "Splunk" :
                    cred_dict["credential"]	= config.get(section, "credential")
                    cred_dict["password"]	= config.get(section, "password")  
            except:
                logging.debug("Exception on option=%s" % option)
                cred_dict["credential"] = None
                cred_dict["password"] = None				
    return cred_dict

def read_splunkConf():
    cred_dict = {}
    app_name = __file__.split(os.sep)[-4]
    app_dir = os.path.join(SPLUNK_HOME, 'etc', 'apps', app_name)
    pswd_input_file = os.path.join(app_dir, 'default', 'passwords.conf')
    config = ConfigParser.ConfigParser()
    config.read(pswd_input_file)
    for section in config.sections():
        options = config.options(section)
        for option in options:
            try:
                if option == "region" and config.get(section, option) == "Splunk" :
                    cred_dict["credential"]	= config.get(section, "credential")
                    cred_dict["password"]	= config.get(section, "password")  
            except:
                logging.debug("Exception on option=%s" % option)
                cred_dict["credential"] = None
                cred_dict["password"] = None				
    return cred_dict	

def create_s3_client():
    try:
        logging.info("Creating boto3 aws session")
        aws_cred = read_passwordConf()
        session = boto3.session.Session(aws_access_key_id=aws_cred["credential"], aws_secret_access_key=aws_cred["password"])
        return session.client('s3')
    except Exception as ex:
        logging.debug(ex)

def upload_to_s3(s3_client,bucket_name,key,file):
    try:
        logging.info("Uploading")
        response = s3_client.put_object(Bucket=bucket_name,Key=key,Body=file);
        logging.info(response)
    except Exception as ex:
        logging.debug(ex)		

def read_inputsConf(aws_account):
    bkt_name = None
    key_name = None
    app_name = __file__.split(os.sep)[-4]
    app_dir = os.path.join(os.environ["SPLUNK_HOME"], 'etc', 'apps', app_name)
    conf_input_file = os.path.join(app_dir, 'default', 'inputs.conf')
    config = ConfigParser.ConfigParser()
    config.read(conf_input_file)
    for section in config.sections():
        #if ADDON_NAME in section:
        options = config.options(section)
        for option in options:
            try:
                if option == "aws_account" and config.get(section,option) == aws_account:
                    section_wrk = section
            except: 
                logging.debug("Exception on option=%s" % option)
    bkt_name = 	config.get(section_wrk, "bucket_name")	
    key_name = 	config.get(section_wrk, "key_name")
    return (bkt_name,key_name)	

def read_exportsConf():
    exprt_conf_dict={}
    app_name = __file__.split(os.sep)[-4]
    app_dir = os.path.join(os.environ["SPLUNK_HOME"], 'etc', 'apps', app_name)
    conf_input_file = os.path.join(app_dir, 'default', 'exports.conf')
    config = ConfigParser.ConfigParser()
    config.read(conf_input_file)
    for section in config.sections():
        if ADDON_NAME in section:
            options = config.options(section)
            for option in options:
                try:
                    if option == "search_query":
                        search_qry = config.get(section, option)
                    if option == "aws_account":
                        aws_acct = config.get(section,option)	
                    exprt_conf_dict[aws_acct] = search_qry					
                except: 
                    logging.debug("Exception on option=%s" % option)
                    bucket_name = None
    return 	exprt_conf_dict				

def prepare_data_to_upload(searchQuery):
    try:
        splunk_cred = read_splunkConf()
        print(splunk_cred["credential"])
        print(splunk_cred["password"])
        cmd = '/opt/splunk/bin/splunk search "' + searchQuery + '" -auth ' + splunk_cred["credential"] + ':' + splunk_cred["password"] + ' -preview 0 -maxout 0 -output rawdata'
        #cmd = '"C:\\Program Files\\Splunk\\bin\\splunk" search "' + searchQuery + '" -auth ' + splunk_cred["credential"] + ':' + splunk_cred["password"] + ' -preview 0 -maxout 0 -output rawdata'
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        logging.info("Raw data received via search request")
        return output
    except Exception as ex:
        logging.debug(ex)	

def gen_key(path, filename):
    try:
        if not path.endswith(os.sep):
            generatedKey = path.strip() + os.sep + filename.strip()
            else:
            generatedKey = path.strip() + filename.strip()
        return generatedKey
        logging.info("Filepath generating")
    except Exception as ex:
        logging.debug(ex)

def prepare_key():
    clientname = "Client"
    prefix = "Prefix"
    date_filestructure = datetime.datetime.now().strftime("%d-%m-%Y")
    date_filename = datetime.datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
    char_set = string.ascii_uppercase + string.digits
    randomStr = ''.join(random.sample(char_set*6, 6))
    filename = clientname + "-" + date_filename + "-" + randomStr + ".log"
    file_path = gen_key(gen_key(prefix, gen_key(clientname, socket.gethostname())), date_filestructure)	
    key = gen_key(file_path, filename)
    return key	

if __name__ == "__main__" :
    exports_dict = {}
    logging.basicConfig(filename='s3_pusher.log',level=logging.INFO)
    logging.info("Starting application...")
    logging.info("Creating S3 client")
    s3_client = create_s3_client()	
    exports_dict = read_exportsConf()
    for aws_account in exports_dict.keys():
        logging.info("Setting variable for bucket name")
        bucket,s3_file_path = read_inputsConf(aws_account)
        logging.info("Preparing data using search query")
        file = prepare_data_to_upload(exports_dict[aws_account])
        key = prepare_key()
        logging.info("Uploading to S3")
        upload_to_s3(s3_client,bucket,key,file)
    

		