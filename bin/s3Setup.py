import json
import splunk.rest

import ConfigParser
from s3Configuration import AppConfiguration
import logging
logging.basicConfig(filename='/opt/splunk/etc/apps/aaam-devops-s3-addon/bin/s3_setup.log',level=logging.INFO)
logging.info("Starting application...")

#####
# REST endpoint setup, available on /setup (see restmap.conf)
#####
class SetupHandler(splunk.rest.BaseRestHandler):

    def __init__(self, method, requestInfo, responseInfo, sessionKey):
        splunk.rest.BaseRestHandler.__init__(self,
            method, requestInfo, responseInfo, sessionKey)
        self.configuration = AppConfiguration()
        logging.info("setup handler init")
        

    def writeJson(self, data):
        self.response.setStatus(200)
        self.response.setHeader('content-type', 'application/json')
        self.response.write(json.dumps(data))
        logging.info("setup handler - write json")

    def handle_GET(self):
        confDict = self.configuration.readConfig()
        confInfo = {}
        if confDict != None:
            for stanza in confDict.sections():
                confInfo[stanza] = {}
                for key, val in confDict.items(stanza):
                    if val in [None]:
                        val = ''
                    confInfo[stanza][key] = val
        self.writeJson(confInfo)
        logging.info("setup handler - handle get")

    def handle_POST(self):
        all_settings = json.loads(self.request["payload"])
        logging.info("setup handler - settings")
        logging.info(all_settings)
        config = ConfigParser.RawConfigParser()
        for stanza in all_settings :
            config.add_section(stanza)
            settings = all_settings[stanza]
            for key in settings :
                config.set(stanza, key, settings[key])
        
        self.configuration.writeConfig(config)
        self.configuration.createInputsConfiguration(configPath=None, fileWriteMode='a')