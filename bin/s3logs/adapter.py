import itertools
from datetime import datetime
from dateutil.tz import tzutc
from botocore.exceptions import ClientError
from splunksdc import logging
from splunksdc.event import EventStream
from splunksdc.multitasking import JobPipelineStop


logger = logging.getLogger('splunk_aws_logs')


class AWSLogsMarkPad(object):
    OVERLAPPED = 500

    def __init__(self, checkpoint):
        self._store = checkpoint.partition('/MK/')

    def read(self):
        return [key for key, _ in self._store.items()]

    def update(self, keys):
        keys = keys[-self.OVERLAPPED:]
        for key in keys:
            self._store.set(key, None)
        markers = [key for key, _ in self._store.items()]
        delta = len(markers) - self.OVERLAPPED
        if delta > 0:
            expired = markers[:delta]
            for key in expired:
                self._store.delete(key)


class JobStrip(object):
    def __init__(self, key, size, etag, mtime, retried=0):
        self.key = key
        self.size = size
        self.etag = etag
        self.mtime = mtime
        self.retried = retried


class PleaseRetry(Exception):
    pass


class AWSLogsJobPad(object):
    def __init__(self, checkpoint):
        self._store = checkpoint.partition('/PS/')

    def add(self, strip):
        key = strip.key
        args = (strip.size, strip.etag, strip.mtime, strip.retried)
        self._store.set(key, args)
        logger.debug('job added', key=key)

    def remove(self, key):
        self._store.delete(key)
        logger.debug('job removed', key=key)

    def jobs(self):
        for key, args in self._store.items():
            job = JobStrip(key, *args)
            yield job


class AWSLogsPipelineAdapter(object):
    _SWEEP_CYCLE = 64
    _EPOCH = datetime(1970, 01, 01, tzinfo=tzutc())

    def __init__(self, app, client, metadata, prefix, marker, sieve, decoder):
        self._app = app
        self._client = client
        self._metadata = metadata
        self._prefix = prefix
        self._marker = marker
        self._sieve = sieve
        self._decoder = decoder

    def is_aborted(self):
        return self._app.is_aborted()

    def discover(self, checkpoint):
        client = self._client
        metadata = self._metadata
        s3 = client.create_s3_client()
        pending = AWSLogsJobPad(checkpoint)
        markpad = AWSLogsMarkPad(checkpoint)

        prefix = self._prefix
        marker = self._marker

        # process failed keys
        yield self._find_failed_jobs(pending)

        # if there are too many failed keys
        # do not try to get more fresh keys
        if self._count_fails(pending) > metadata.max_fails:
            logger.error('too many items in retrying queue')
            yield JobPipelineStop(True)

        for cycle in itertools.count():
            if self.is_aborted():
                yield JobPipelineStop(True)

            if client.need_retire():
                logger.info('aws credentials will expire soon.')
                yield JobPipelineStop(False)

            if cycle % self._SWEEP_CYCLE == 0:
                checkpoint.sweep()

            # get more fresh keys
            markers = markpad.read()
            if not markers:
                markers.append(marker)
            marker = markers[0]
            files = client.list_files(s3, prefix, marker)
            fresh_files = self._find_fresh_files(files, markers)

            for item in fresh_files:
                key = item['Key']
                size = item['Size']
                etag = item['ETag'].lower()
                last_modified = item['LastModified']
                delta_to_epoch = last_modified - self._EPOCH
                mtime = int(delta_to_epoch.total_seconds())
                logger.info('fresh file found', key=key)
                strip = JobStrip(key, size, etag, mtime)
                pending.add(strip)
                markers.append(key)
            
            markpad.update(markers)

            # process fresh keys
            yield self._find_fresh_jobs(pending)

            # no more fresh key
            if not fresh_files:
                yield JobPipelineStop(True)

    def do(self, job, s3):
        # will retry except 404
        try:
            content = self._fetch(job, s3)
        except Exception as e:
            logger.exception('job downloading error')
            if isinstance(e, ClientError):
                code = e.response['Error'].get('Code', 'Unknown')
                if code == 'NoSuchKey':
                    return e
            return PleaseRetry()

        # never retry
        try:
            records = self._decoder(content)
            return records
        except Exception as e:
            logger.exception('job decoding error')
            return e

    def done(self, portal, checkpoint, job, result):
        metadata = self._metadata
        pending = AWSLogsJobPad(checkpoint)
        if not isinstance(result, Exception):
            logger.info("job success", key=job.key, count=len(result))
            self._index_records(portal, job, result)
            pending.remove(job.key)
            return

        if isinstance(result, PleaseRetry):
            job.retried += 1
            if metadata.should_retry(job.retried):
                pending.add(job)
                logger.info('job will retry', key=job.key, retried=job.retried)
                return

        # fall through
        logger.warning('job failed', key=job.key)
        pending.remove(job.key)

    def allocate(self):
        client = self._client
        s3 = client.create_s3_client()
        return s3

    def _fetch(self, job, s3):
        bucket = self._client.bucket_name
        response = s3.get_object(
            Bucket=bucket,
            Key=job.key
        )
        body = response['Body']
        content = body.read()
        body.close()
        return content

    def _index_records(self, portal, job, records):
        metadata = self._metadata
        bucket_name = self._client.bucket_name
        key = job.key
        source = 's3://' + bucket_name + '/' + key
        stream = EventStream(
            index=metadata.index,
            host=metadata.host,
            stanza=metadata.stanza,
            sourcetype=metadata.sourcetype,
            source=source,
        )
        for data in records:
            stream.append(data)
        portal.write(stream)
        logger.info("sent for indexing", key=key)

    def _find_fresh_files(self, files, markers):
        fresh_files = list()
        files = self._sieve(files)
        for item in files:
            key = item['Key']
            if key in markers:
                logger.debug('ignore known file', key=key)
                continue
            if key.endswith('/'):
                logger.debug('ignore placeholder', key=key)
                continue
            size = item['Size']
            if size == 0:
                logger.debug('ignore empty file', key=key)
                continue
            fresh_files.append(item)
        return fresh_files

    @classmethod
    def _count_fails(cls, pad):
        count = 0
        for item in pad.jobs():
            if item.retried > 0:
                count += 1
        return count

    @classmethod
    def _find_failed_jobs(cls, pad):
        return [item for item in pad.jobs() if item.retried > 0]

    @classmethod
    def _find_fresh_jobs(cls, pad):
        return [item for item in pad.jobs() if item.retried == 0]
