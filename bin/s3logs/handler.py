from collections import namedtuple
import botocore.exceptions
import botocore.endpoint
from splunksdc import logging
from splunksdc import JobPipeline
from s3logs.adapter import AWSLogsPipelineAdapter


logger = logging.getLogger('splunk_aws_logs')


AWSLogsTask = namedtuple('AWSLogsTask', ('name', 'params'))


class AWSLogsHandler(object):
    _EXCEPTIONS = (
        IOError,
        botocore.exceptions.BotoCoreError,
        botocore.exceptions.ClientError,
    )

    @classmethod
    def build(cls, client_builder, metadata, delegate):
        return cls(client_builder, metadata, delegate)

    def __init__(self, client_builder, metadata, delegate):
        self._metadata = metadata
        self._delegate = delegate
        self._client_builder = client_builder

    def prepare(self, scheduler):
        metadata = self._metadata
        params = metadata.__dict__
        try:
            logger.info('prepare started', **params)
            client = self._client_builder()
            tasks = self._delegate.create_tasks(client, metadata)
            for name, params in tasks:
                scheduler.add_task(name, params, 0)
        except self._EXCEPTIONS as e:
            logger.exception('prepare error')

        logger.info('prepare end')

    def perform(self, app, portal, checkpoint, name, params):
        try:
            logger.info("perform start", name=name, params=params)
            client = self._client_builder()
            metadata = self._metadata
            prefix = self._delegate.create_prefix(name, params)
            marker = self._delegate.create_initial_marker(name, params)
            philter = self._delegate.create_filter()
            decoder = self._delegate.create_decoder()

            while not app.is_aborted():
                if client.need_retire():
                    client = self._client_builder()

                adapter = AWSLogsPipelineAdapter(
                    app, client, metadata,
                    prefix, marker, philter, decoder
                )
                pipeline = JobPipeline(adapter)
                if pipeline.run(portal, checkpoint):
                    # no more new files
                    break
        except self._EXCEPTIONS as e:
            logger.exception('perform error')

        logger.info('perform end')
