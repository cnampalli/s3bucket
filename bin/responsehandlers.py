# add your custom response handler class to this module
import datetime
import json
import logging
import requests
import time


# the default handler , does nothing , just passes the raw output directly to STDOUT
class DefaultResponseHandler:
    def __init__(self, **args):
        pass

    def __call__(self, response_object, raw_response_output, response_type, req_args, endpoint):
        cookies = response_object.cookies
        if cookies:
            req_args["cookies"] = cookies

        if not "params" in req_args:
            req_args["params"] = {}

        req_args["params"]["dateStart"] = "hello";
        print_xml_stream(raw_response_output)


class s3:
    def __init__(self, **args):
        pass

    def __call__(self, response_object, raw_response_output, response_type, req_args, endpoint):
        if response_type == "json":
            logging.info("Response : %s" % raw_response_output)

            # TODO index response

        else:
            print_xml_stream(raw_response_output)


# HELPER FUNCTIONS

# prints XML stream
def print_xml_stream(s):
    print "<stream><event unbroken=\"1\"><data>%s</data><done/></event></stream>" % encodeXMLText(s)


def print_xml_stream_with_time(s, date):
    timestamp = time.mktime(datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%SZ").timetuple())
    print "<stream><event unbroken=\"1\"><time>%s</time><data>%s</data><done/></event></stream>" % (
        encodeXMLText(str(timestamp)), encodeXMLText(s))


def encodeXMLText(text):
    text = text.replace("&", "&amp;")
    text = text.replace("\"", "&quot;")
    text = text.replace("'", "&apos;")
    text = text.replace("<", "&lt;")
    text = text.replace(">", "&gt;")
    text = text.replace("\n", "")
    return text
